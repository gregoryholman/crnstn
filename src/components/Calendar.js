import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import styled from 'styled-components';

import 'react-datepicker/dist/react-datepicker.css';

const DPWrapper = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  margin: 20px;
`;

const Prompt = styled.div`
  margin-right: 10px
`;

export class Calendar extends Component {
  constructor(props) {
    super(props);
    this.setDate = this.setDate.bind(this);
  }

  /**
   * Set a new date from the picker
   * @param {Moment} date - the date to be set
   */
  setDate(date) {
    this.props.setDate(date, true); // Update the date, and add a history entry
  }

  render() {
    return (
     <DPWrapper>
       <Prompt>Please select a date:</Prompt>
       <DatePicker
         selected={this.props.date}
         onChange={this.setDate}
         minDate={moment('1995-06-16')} // Start date according to the API
         maxDate={moment()} // Of course, can't have APOD for the future
     /></DPWrapper>
    )
  }
}