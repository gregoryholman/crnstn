import React, {Component} from 'react';
import styled from 'styled-components';

const Button = styled.a`
  background: white;
  color: black;
  display: inline-block;
  font-weight: bold;
  margin-top: 15px;
  padding: 10px 20px; 
  text-decoration: none;
  text-transform: uppercase;
`;

const Img = styled.img`
  display: block;
  margin: 0 auto;
  max-width: 100%
`;

export class Image extends Component {

  getImage() {
    const imageOfTheDayDetails = this.props.imageOfTheDayDetails;
    let isValidImage = imageOfTheDayDetails && imageOfTheDayDetails.url && imageOfTheDayDetails.media_type === 'image';
    let isVideo = imageOfTheDayDetails && imageOfTheDayDetails.url && imageOfTheDayDetails.media_type === 'video';
    if (isValidImage) {
      return (
          <Img src={imageOfTheDayDetails.url} alt=''/> // I don't tend to use alt tag values for presentational images; only where they have semantic value
      )
    } else {
      return (
          <div>
            <p>Ah, there's not a valid image for today</p>

            {isVideo && // Some are youtube videos, I discovered - 29/07/2018 for example
            <div>
              <p>But there is a video!</p>
              <Button target={'_blank'} href={imageOfTheDayDetails.url}>View it</Button>
            </div>
            }
          </div>)
    }
  }

  render() {
    const image = this.getImage();
    return (
        <div>
          {image}
        </div>

    )
  }
}