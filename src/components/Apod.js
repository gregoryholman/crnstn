import React, {Component} from 'react';
import moment from "moment/moment";

import {Image} from "./Image";
import {Calendar} from "./Calendar";
import styled from "styled-components";

const Title = styled.h1`
  color: black;
  font-size: 30px;
  margin: 20px;
  text-align: center;
`;

const Overlay = styled.div`
  align-items: center;
  background: rgba(0, 0, 0, 0.3);
  color: black;
  display: flex;
  height: 100vh;
  justify-content: center;
  opacity: ${props => props.loading ? '1' : '0'};
  pointer-events: none;
  position: fixed;
  transition: opacity .3s ease-out;
  top: 0;
  width: 100%;
  z-index: 5;
`;

const Loading = styled.img`
  height: 90px;
  width: 120px;
`;

const Profile = styled.div`
  background: url('/images/Greg_small.jpg') no-repeat center / cover
  border: 8px solid white;
  border-radius: 50%;
  height: 300px;
  font-size: 30px;
  margin: 0 auto;
  width: 300px;
`;

const Body = styled.div`
  background-color: #3cc2cb;
  min-height: 100vh;
  padding: 50px 0;
`;

export class Apod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      imageOfTheDayDetails: null,
      loading: false
    };
    this.setDate = this.setDate.bind(this);
  }

  /**
   * Set an image for the current date
   * @param {string} date - the formatted date to set
   */
  setImageForDate(date) {
    // Check the local fetchedImages for a match
    const matchedStoredImage = this.props.fetchedImages.find(image => image.date === date);
    if (matchedStoredImage) { // We already have an image; use it
      this.setState({
        imageOfTheDayDetails: matchedStoredImage
      });
    } else { // No match; perform a fetch for the image details
      this.setState({loading: true});
      const apiKey = '?api_key=dsJRQIHiFsQBcsamVkuYR2CFTpM1w44mbEc62PNG';
      const dateQueryParam = `&date=${date}`;
      const fetchUrl = `https://api.nasa.gov/planetary/apod${apiKey}${dateQueryParam}`;
      fetch(fetchUrl)
          .then(res => res.json())
          .then(
              (imageOfTheDayDetails) => {
                this.props.addToFetchedImages(imageOfTheDayDetails);
                this.setState({
                  imageOfTheDayDetails: imageOfTheDayDetails
                });
                this.setState({loading: false});
              },
              (error) => {
                this.setState({
                  error: true,
                  errorDetails: JSON.stringify(error, false, ' ')
                });
                this.setState({loading: false});
              }
          );
    }
  }

  /**
   *
   * @param {Moment} date - the date to set
   * @param {boolean} addHistory - add a history entry for this date
   */
  setDate(date, addHistory) {
    // Update the date right away otherwise the UI looks laggy when we perform a network request
    this.setState({
      date: date
    });
    const formattedDate = date.format('YYYY-MM-DD'); // This is how the API needs it
    if (addHistory) {
      this.props.history.push(`/${formattedDate}`);
    }
    this.setImageForDate(formattedDate);
  }

  render() {
    return (
        <div>
          <Body className="App">
          <Overlay loading={this.state.loading}><Loading src={'/images/loading-icon.gif'} alt={'Loading Icon'}/></Overlay>
          <header>
            <Profile/>
            <Title>Greg Holman - Cornerstone Frontend Test</Title>
          </header>
          <main>
            <Calendar setDate={this.setDate} date={this.state.date}/>
            <Image imageOfTheDayDetails={this.state.imageOfTheDayDetails}/>
          </main>
          </Body>
        </div>
    )
  }

  componentDidMount() {
    // check if we've been passed a date, or set it to today
    let date = this.props.match && this.props.match.params && this.props.match.params.date;
    date = date ? moment(date) : moment();
    this.setDate(moment(date), false);
  }
}