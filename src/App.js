import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

// Component imports
import {Apod} from "./components/Apod";

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchedImages: [],
    };
    this.addToFetchedImages = this.addToFetchedImages.bind(this);
  }

  addToFetchedImages(newImage) {
    this.setState(prevState => {
      let fetchedImages = ([...prevState.fetchedImages, newImage]);
      return {fetchedImages: fetchedImages};
    })
  }

  render() {
    return (
        <Router>
          <Switch>
            <Route exact={false} path="/:date?" render={(props) => (
                <Apod
                    {...props}
                    fetchedImages={this.state.fetchedImages}
                    addToFetchedImages={this.addToFetchedImages}
                />
            )}/>
          </Switch>
        </Router>
    );
  }
}

export default App;
